package main

import (
	"github.com/r3labs/sse"
	"net/http"
	"strconv"
	"time"
)

func main() {
	// SSE
	sseServer := sse.New()
	sseServer.AutoReplay = false
	sseServer.CreateStream("messages")

	// static
	fs := http.FileServer(http.Dir("frontend"))

	// Create a new Mux and set the handlers
	mux := http.NewServeMux()
	mux.HandleFunc("/events", sseServer.HTTPHandler)
	mux.Handle("/", fs)

	go dataSender(sseServer)

	http.ListenAndServe("localhost:8080", mux)
}

func dataSender(server *sse.Server) {
	i := 0

	for {
		server.Publish("messages", &sse.Event{
			Data: []byte(strconv.Itoa(i)),
		})
		i++
		time.Sleep(2 * time.Second)
	}
}
